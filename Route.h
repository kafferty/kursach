#ifndef ROUTE_H_
#define ROUTE_H_

#include <iostream>
#include <fstream>
#include <map>
#include "TimetableElement.h"

class Route {
    public:
    std::map <int, TimetableElement> TimetableMap;

    Route(){};
    ~Route(){TimetableMap.clear();};
    void table();

    void ReadRouteFile(); //Считывание в карту из файла
    void EntryRouteFile(); //Добавление маршрута в карту

    void ShowRoute(); //Выведение маршрута на экран

    void ShowRouteNumber(const int &vubor); //Выведение маршрута по его номеру

    void DeleteRoute();//Удаление маршрута
    void RedaktRoute();//Редактирование маршрута

    void FindRouteNumber();//Поиск маршрута по номеру

    void FindStationFrom();//Поиск по станциям отправления

    void FindStationTo();//Поиск по станциям прибытия

    void FindDateFrom();//Поиск по дате отправления

};
#endif /* ROUT_H_ */
