#include "TimetableElement.h"
#include <iomanip>

using namespace std;

   istream& operator>>(std::istream& ist, Time& time) {//Перегрузка оператора ввода для Time
    char skip = 'x';
    ist >> time.hours >> skip >> time.minutes;
    if(time.hours < 0 || time.hours > 24) {
        ist.setstate(std::ios::failbit);
    };
    if(skip != ':') ist.setstate(std::ios::failbit);
    if(time.minutes < 0 || time.minutes >59) {
        ist.setstate(std::ios::failbit);
    }

    return ist;
}

   istream& operator>>(std::istream& ist, Date& date) {//Перегрузка оператора ввода для Date
    char skip1 ='x', skip2 = 'x';
    ist >> date.day >> skip1 >> date.month >> skip2 >> date.year;
    if(date.day < 1 || date.day > 31) {
        ist.setstate(std::ios::failbit);

    };
    if (skip1!='.') ist.setstate(std::ios::failbit);
    if(date.month < 1 || date.month > 12) {
        ist.setstate(std::ios::failbit);
    };
    if (skip2!='.') ist.setstate(std::ios::failbit);
    if (date.year <2015 || date.year > 2030) {
        ist.setstate(std::ios::failbit);
    };
    return ist;

}
    ofstream &operator <<(ofstream &add, const TimetableElement &z) {//Перегрузка оператора записи для Timetable

        add<<z.RouteNumber<<" ";
        add<<z.StationFromStationTo<<" ";
        add<<z.timefrom<<"-";
        add<<z.timeto<<" ";
        add<<z.datefrom<<"-";
        add<<z.dateto;
        add<<endl;
        return add;
    }

    ofstream &operator <<(ofstream &get, const Time &time) {//Перегрузка оператора записи для Time
        get<<time.hours;
        get<<":";
        get<<time.minutes;
        return get;
    }

    ofstream &operator <<(ofstream &add, const Date &date) {//Перегрузка оператора записи для Date
        add<<date.day;
        add<<".";
        add<<date.month;
        add<<".";
        add<<date.year;
        return add;
    }

    ifstream &operator >>(ifstream &get, TimetableElement &z) {//Перегрузка оператора чтения для TimetableElement
        char skip;
        get>>z.RouteNumber;
        get>>z.StationFromStationTo;
        get>>z.timefrom>>skip;
        get>>z.timeto;
        get>>z.datefrom>>skip;
        get>>z.dateto;
            return get;
    }

    ifstream &operator >>(ifstream &get, Time &time) {//Перегрузка оператора чтения для Time
        char skip;
        get>>time.hours;
        get>>skip;
        get>>time.minutes;
        return get;
    }

    ifstream &operator >>(ifstream &add, Date &date) {//Перегрузка оператора чтения для Date
        char skip;
        add>>date.day;
        add>>skip;
        add>>date.month;
        add>>skip;
        add>>date.year;
        return add;
    }

    ostream &operator <<(ostream &add, const TimetableElement &z){//Перегрузка оператора вывода для TimetableElement
        add<<"| "<<setw(5)<<setfill(' ')<<z.RouteNumber;
        add<<"| "<<setw(26)<<setfill(' ')<<z.StationFromStationTo<<" ";
        add<<"| "<<setw(10)<<z.timefrom<<"-"<<z.timeto;
        add<<"| "<<setw(26)<<z.datefrom<<"-"<<z.dateto<<"   |";
        add<<endl;
        cout<<"----------------------------------------------------------------------------" <<endl;

        return add;
    }

    ostream &operator <<(ostream &ist, const Time &time){//Перегрузка оператор вывода для Time
        ist.setf(ios::right);
        ist<<setw(2)<<setfill('0')<<time.hours;
        ist<<":";
        ist<<setw(2)<<setfill('0')<<time.minutes<<" ";
        return ist;
    }

    ostream &operator <<(ostream &ist, const Date &date){//Перегрузка оператор вывода для Date
        ist.setf(ios::right);
        ist<<setw(2)<<date.day;
        ist<<".";
        ist<<setw(2)<<date.month;
        ist<<".";
        ist<<setw(2)<<date.year<<" ";
        return ist;
    }

    istream &operator >>(istream &get, TimetableElement &z){//Перегрузка оператор ввода для TimetableElement
        char skip;
        get>>z.RouteNumber;
        get>>z.StationFromStationTo;
        get>>z.timefrom;
        get>>skip;
        get>>z.timeto;
        get>>z.datefrom;
        get>>skip;
        get>>z.dateto;
        return get;
    }

    TimetableElement TimetableElement::operator = (const TimetableElement &z)//Оператор присваивания для TimetableElement
    {
        StationFromStationTo = z.StationFromStationTo;
        RouteNumber = z.RouteNumber;
        timeto=z.timeto;
        timefrom=z.timefrom;
        datefrom=z.datefrom;
        dateto=z.dateto;
            return z;
    }

    TimetableElement::TimetableElement (const TimetableElement &z)//Оператор копирования для TimetableElement
    {
             RouteNumber=z.RouteNumber;
             StationFromStationTo = z.StationFromStationTo;
             timeto=z.timeto;
            timefrom=z.timefrom;
            datefrom=z.datefrom;
            dateto=z.dateto;
    }


    Time Time::operator = (const Time &z)//Оператор присваивания для Time
    {
        hours=z.hours;
        minutes=z.minutes;
        return z;

    }

    Time::Time(const Time &z)//Оператор копирования для Time
    {
        hours=z.hours;
        minutes=z.minutes;
    }

    Date Date::operator = (const Date &z)//Оператор присваивания для Date
    {
        day=z.day;
        month=z.month;
        year=z.year;
        return z;

    }

    bool operator == (const Date &datefrom, const Date &date2) //Оператор сравнения
    {
        if (datefrom.day==date2.day) {
            if (datefrom.month==date2.month) {
                if (datefrom.year==date2.year) return true;
            }
        }
        return false;
    }

    Date::Date(const Date &z)//Оператор копирования для Date
    {
        day=z.day;
        month=z.month;
        year=z.year;
    }
