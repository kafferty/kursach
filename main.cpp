#include <iostream>
#include "Route.h"
#include <stdlib.h>

using namespace std;
int main (void)
{
    Route r;
    int stroka;
    cout<<"*WELCOME TO THE T_I_M_E_T_A_B_L_E!*"<<endl;
        while (true)
        {
            stroka=0;
            cout<<endl;
            cout<<"------------------------------------------------------"<<endl;
            cout<<"|0) About this programm                              |"<<endl;
            cout<<"|1) Add Route                                        |"<<endl;
            cout<<"|2) Delete Route                                     |"<<endl;
            cout<<"|3) Edit Timetable                                   |"<<endl;
            cout<<"|4) Show Timetable                                   |"<<endl;
            cout<<"|5) Search by route number                           |"<<endl;
            cout<<"|6) Search the departure station                     |"<<endl;
            cout<<"|7) Search the arrival station                       |"<<endl;
            cout<<"|8) Search by date of departure                      |"<<endl;
            cout<<"|9) Exit                                             |"<<endl;
            cout<<"------------------------------------------------------"<<endl<<endl;
            r.ReadRouteFile();
            cout<<"Your choice:"<<endl;
            cin>>stroka;

            switch(stroka)
           {
                   case 0:
                           cout<<"This programm is designed for edit Train Timetable. "<<endl;
                           cout<<"Simplicity and ease of use certainly you will like!"<<endl;
                           cout<<"P.S. I hope!"<<endl;
                           cout<<"P.S. Have a good day!"<<endl;

                    case 1: r.EntryRouteFile();
                       r.table();
                        r.ShowRoute(); break;//Выведение нового расписания на экран
                    case 2: r.DeleteRoute();
                        r.table();
                         r.ShowRoute(); break; //Выведение нового расписания на экран
                    case 3: r.RedaktRoute();
                        r.table();
                        r.ShowRoute(); break;
                    case 4: r.table();
                            r.ShowRoute(); break;//Просмотр расписания
                    case 5: r.FindRouteNumber();break;
                    case 6: r.FindStationFrom(); break;
                    case 7: r.FindStationTo(); break;
                    case 8: r.FindDateFrom(); break;
                    case 9: cout<<"Goodbye! See you later!"<<endl;
                            exit(0);
                            break;
                    default: cout<<"No such item!"; break;
           };
        };
    return 0;
};
