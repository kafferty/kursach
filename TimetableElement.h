#ifndef TIMETABLEELEMENT_H
#define TIMETABLEELEMENT_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>



struct Time {
    int hours;
    int minutes;

    friend std::istream& operator >>(std::istream& ist, Time& time);
    friend std::ostream& operator <<(std::ostream& ist, const Time& time);
    friend std::ofstream &operator <<(std::ofstream &get, const Time &time);
    friend std::ifstream &operator >>(std::ifstream &get, Time &time);
    Time operator = (const Time &time);

    Time (const Time &time);

    Time (){};
    ~Time (){};

};

struct Date {
    int day;
    int month;
    int year;

    friend std::istream& operator>>(std::istream& ist, Date& date);
    friend std::ostream& operator <<(std::ostream& ist, const Date& date) ;
    friend std::ofstream &operator <<(std::ofstream &add, const Date &date);
    friend std::ifstream &operator >>(std::ifstream &add, Date &date);
    Date operator = (const Date &date);
    friend bool operator == (const Date &date1, const Date &date2);

    Date (const Date &time);

    Date (){};
    ~Date (){};
};

class TimetableElement { //Структура Расписание

public:
    std::string StationFromStationTo;
    int RouteNumber;
    Date datefrom, dateto;
    Time timefrom, timeto;


    friend std::ofstream &operator <<(std::ofstream &add, const TimetableElement &z);

    friend std::ifstream &operator >>(std::ifstream &get, TimetableElement &z);

    friend std::ostream &operator <<(std::ostream &add, const TimetableElement &z);

    friend std::istream &operator >>(std::istream &get, TimetableElement &z);

    TimetableElement operator = (const TimetableElement &z);

    TimetableElement (const TimetableElement &z);

    TimetableElement (){};
    ~TimetableElement (){};

};
#endif
