#include "Route.h"
using namespace std;

static void recoverCin() {
    std::cin.clear();
    std::cin.ignore(0xFFFFFFF, '\n');
    std::cout << "Incorrect input, please try again" << std::endl;
    std::cin.get();
}

void Route::table()
{
    cout<<"|--------------------------------------------------------------------------|\n";
    cout<<"|Route | Station From and Station To|    Time      |      Date             |\n";
    cout<<"|Number|                            |              |                       |\n";
    cout<<"|------|----------------------------|--------------|-----------------------|\n";
}
    void Route:: ReadRouteFile()
    {//Считывание в карту из файла
        std::ifstream in;
        in.open("/Users/lidochka/TimetableMeow/Route.txt");
        while (in)
        {
            TimetableElement px;
            in>>px;
            if (px.StationFromStationTo!="") {
            TimetableMap.insert(pair<int, TimetableElement> (px.RouteNumber, px));
            }
        }
        in.close();
    }

    void Route:: EntryRouteFile()
    {//Добавления маршрута в карту
        TimetableElement t;
        Date date;
        char skip;
        std::cout<<"Station from and Station to ";std::cin>>t.StationFromStationTo;
        if(!std::cin) {
            recoverCin();
            return;
        }

        std::cout<<"Route number ";std::cin>>t.RouteNumber;
        if(!std::cin) {
            recoverCin();
            return;
        }

        std::cout<<"Time ";
        std::cin >> t.timefrom
                 >> skip
                 >> t.timeto;
        if(!std::cin) {
            recoverCin();
            return;
        }

        std::cout<<"Date ";
        std::cin >> t.datefrom
                 >> skip
                 >> t.dateto;
        if(!std::cin) {
            recoverCin();
            return;
        }

        std::cout << std::endl;

        if(!std::cin) recoverCin();
        else {
            TimetableMap.insert(pair<int, TimetableElement> (t.RouteNumber, t));
            std::ofstream add;
            add.open("/Users/lidochka/TimetableMeow/Route.txt");
            typedef map<int, TimetableElement>::const_iterator MapIterator;
            for (MapIterator iter = TimetableMap.begin(); iter!=TimetableMap.end(); iter++)
            {
                add << iter->second;
            }
            add.close();
        }
    }

    void Route:: ShowRoute()
    {//Выведение маршрутов на экран
        typedef map<int, TimetableElement>::const_iterator MapIterator;
        for (MapIterator iter = TimetableMap.begin(); iter!=TimetableMap.end(); iter++)
        {
            cout << iter->second;
        }

    }

    void Route:: ShowRouteNumber(const int &vubor)
    {//Выведение маршрута по его номеру
        typedef map<int, TimetableElement>::const_iterator MapIterator;
        for (MapIterator iter = TimetableMap.begin(); iter!=TimetableMap.end(); iter++)
        {
            if(iter->first==vubor)
            cout << iter->second;
        }
    }

    void Route:: DeleteRoute()
    {//Удаление маршрута
        typedef map<int, TimetableElement>::const_iterator MapIterator;
        int vubor;
        cout<<"Which route you want to delete?"<<endl;
        cin>>vubor;
        TimetableMap.erase(vubor);
        std::ofstream add;
        add.open("/Users/lidochka/TimetableMeow/Route.txt");
        typedef map<int, TimetableElement>::const_iterator MapIterator;

        for (MapIterator iter = TimetableMap.begin(); iter!=TimetableMap.end(); iter++)
        {
            add << iter->second;
        }
        add.close();
    }

    void Route:: RedaktRoute()
    {//Редактирование маршрута
        int vubor;
        cout<<"Enter Route Number:";
        cin>>vubor;
        TimetableElement t;
        char skip;

        cout<<"New Route Number:"; cin>>t.RouteNumber;
        if(!std::cin) {
            recoverCin();
            return;
        }
        cout<<"Station From and Station To:";cin>>t.StationFromStationTo;
        if(!std::cin) {
            recoverCin();
            return;
        }
        cout<<"Time From and Time To:";cin>>t.timefrom>>skip>>t.timeto;
        if(!std::cin) {
            recoverCin();
            return;
        }
        cout<<"Date"; cin>>t.datefrom>>skip>>t.dateto;
        if(!std::cin) {
            recoverCin();
            return;
        }
        TimetableMap.erase(vubor);
        TimetableMap.insert(pair<int, TimetableElement> (t.RouteNumber, t));
        std::ofstream add;
        add.open("/Users/lidochka/TimetableMeow/Route.txt");
        std::ofstream("/Users/lidochka/TimetableMeow/Route.txt");
        typedef map<int, TimetableElement>::const_iterator MapIterator;
        for (MapIterator iter = TimetableMap.begin(); iter!=TimetableMap.end(); iter++)
        {
            add << iter->second;
        }
        add.close();

    }

    void Route:: FindRouteNumber()
    {//Поиск маршрута по номеру
        int vubor;
        cout<<"Enter Route Number:";
        cin>>vubor;
        cout<<"Retrieved route:";
        typedef map<int, TimetableElement>::const_iterator MapIterator;
        MapIterator it = TimetableMap.find(vubor);
        if(it == TimetableMap.end()) {
            cout << "Incorrect route" << std::endl;
        } else cout << TimetableMap.find(vubor)->second;
    }

    void Route:: FindStationFrom()
    {//Поиск маршрута по станции отправления
        bool Bool=false;
        string vubor;
        cout<<"Enter Station From"<<endl;
        cin>>vubor;
        typedef map<int, TimetableElement>::const_iterator MapIterator;
        for (MapIterator iter = TimetableMap.begin(); iter!=TimetableMap.end(); iter++)
        {
            if((*iter).second.StationFromStationTo.find(vubor) == 0)
            {
               Bool=true;
               cout << iter->second;
            }
        }
        if (Bool!=true) cout<<"No such departure station!"<<endl;
    }

    void Route:: FindStationTo()
    {//Поиск маршрута по станции прибытия
        bool Bool=false;
        string vubor;
        cout<<"Enter Station To:"<<endl;
        cin>>vubor;
        typedef map<int, TimetableElement>::const_iterator MapIterator;
        for (MapIterator iter = TimetableMap.begin(); iter!=TimetableMap.end(); iter++)
        {
            if(((*iter).second.StationFromStationTo.find(vubor) != string::npos) &&((*iter).second.StationFromStationTo.find(vubor) != 0) )
            {
                Bool=true;
                cout << iter->second;
            }

        }
        if (Bool!=true) cout<<"No such arrival station!"<<endl;
    }

    void Route::FindDateFrom()
    {//Поиск по дате отправления
        bool Bool=false;
        Date vubor;
        cout<<"Enter date:"<<endl;
        cin>>vubor;
        if(!std::cin) {
            recoverCin();
            return;
        }

        typedef map<int, TimetableElement>::const_iterator MapIterator;
        for (MapIterator iter = TimetableMap.begin(); iter!=TimetableMap.end(); iter++)
        {
            if((*iter).second.datefrom == vubor)
            {
                Bool = true;
                cout<<iter->second<<endl;
            }
         }
         if (Bool!=true) cout<<"Takoi datu net"<<endl;
    }

